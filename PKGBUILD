# Maintainer: Philip Mueller <philm@manjaro.org>
# Contributor: Danct12 <danct12@disroot.org>
# Contributor: Furkan K. <furkan@fkardame.com>
# Contributor: Dragan Simic <dsimic@buserror.io>

pkgname=uboot-pinephone
pkgver=2023.04
pkgrel=1
_tfaver=lts-v2.8.4
_scpver=0.5
pkgdesc="U-Boot for Pine64 PinePhone with CRUST support"
arch=('aarch64')
url='http://www.denx.de/wiki/U-Boot/WebHome'
license=('GPL')
depends=('uboot-tools')
makedepends=('bc' 'python' 'swig' 'dtc' 'arm-none-eabi-gcc' 'or1k-elf-gcc' 'or1k-elf-binutils' 'bison' 'flex' 'python-setuptools')
provides=('uboot')
conflicts=('uboot')
backup=('boot/boot.txt' 'etc/pinephone/uboot.conf')
install=${pkgname}.install

source=("uboot-${pkgver/rc/-rc}.tar.gz::https://github.com/u-boot/u-boot/archive/refs/tags/v${pkgver/rc/-rc}.tar.gz"
        "arm-trusted-firmware-${_tfaver}.tar.gz::https://github.com/ARM-software/arm-trusted-firmware/archive/refs/tags/${_tfaver}.tar.gz"
        "crust-${_scpver}.tar.gz::https://github.com/crust-firmware/crust/archive/refs/tags/v${_scpver}.tar.gz"
        '1001-pinephone-Add-volume_key-environment-variable.patch'
        '1002-Enable-led-on-boot-to-notify-user-of-boot-status.patch'
        '1003-mmc-sunxi-Add-support-for-DMA-transfers.patch'
        '1004-mmc-sunxi-DDR-DMA-support-for-SPL.patch'
        #'1005-spl-ARM-Enable-CPU-caches.patch'
        '1006-common-expose-DRAM-clock-speed.patch'
        '1007-Improve-Allwinner-A64-timer-workaround.patch'
        #'2001-Don-t-enable-all-referenced-regulators.patch'
        'boot.txt'
        'pp-uboot-mkscr'
        'pp-uboot-flash'
        'uboot.conf'
        'pp-prepare-fstab'
        'pp-prepare-fstab.service')
sha256sums=('98ccc5ea7e0f708b7e66a0060ecf1f7f9914d8b31d9d7ad2552027bd0aa848f2'
            '9887ca101cfa2d4efe2af9d2671aa64b9ba82a7f28409e5854696a7778b35225'
            '8b23b2649bbd19dfb84ae00b2419539b8236c6ae9a380beff7dffafd3f41f31b'
            'c1f678d459eba55dae09619df28250b4acf15effe3159962cc7d566ee8fffc62'
            '85cbfd5ba7100f63b2fd364fe2de7bea3fed0aee86e6d6b3590c5734de3f2b85'
            '3858618c09cca2cd9ba1a0eed573b701700c92beedab56db380808e4c50dca49'
            'a23ba08b39510862751602726350d658c1c78cdb9875047fea6678078f51dda1'
            'a73ed43ebb183e1081cb60924cde3cf1e4e6b4dca3a8bb553b6a9121e2e36b27'
            'c9a8c10fa5d2ef837e029a6ca88e7ea3c4e8fbf84da5999577065aab1797a7ed'
            'a748162cee18a52a245c1ac1c56a0821ab8eb01f51d10835d56b86ad8e80507a'
            '0e1e2c996e923330d6f6aec36a0bc4a190a3ff1ccf41f20c51c7b5af10793fcf'
            'efa9a4d74b45ce07d363e0f637d016931b5d22bb7823e1d4c26432b9845551aa'
            'fa13fc36498b29580449d1ad5d35d03e59cbf7ff845c0f7578463c35cb2de546'
            '8e65f03e5e3f2f5b108f73daa17d84f63bf242f039b8827139022cfe043b29be'
            '69c5c7dddf9fe138985a9fe83d04d02045631a6d3945a364b24b116f96cabf00')

prepare() {
  apply_patches() {
      local PATCH
      for PATCH in "${source[@]}"; do
          PATCH="${PATCH%%::*}"
          PATCH="${PATCH##*/}"
          [[ ${PATCH} = $1*.patch ]] || continue
          msg2 "Applying patch: ${PATCH}..."
          patch -N -p1 < "../${PATCH}"
      done
  }

  cd u-boot-${pkgver/rc/-rc}

  # U-Boot patches specific to the PinePhone
  apply_patches 1

  cd ../arm-trusted-firmware-${_tfaver}
  
  # TFA patches specific to the PinePhone
  apply_patches 2
}

build() {
  # Avoid build warnings by editing a .config option in place instead of
  # appending an option to .config, if an option is already present
  update_config() {
    if ! grep -q "^$1=$2$" .config; then
      if grep -q "^# $1 is not set$" .config; then
        sed -i -e "s/^# $1 is not set$/$1=$2/g" .config
      elif grep -q "^$1=" .config; then
        sed -i -e "s/^$1=.*/$1=$2/g" .config
      else
        echo "$1=$2" >> .config
      fi
    fi
  }

  unset CFLAGS CXXFLAGS CPPFLAGS LDFLAGS

  cd crust-${_scpver}

  echo -e "\nBuilding CRUST for Pine64 PinePhone...\n"
  make CROSS_COMPILE=or1k-elf- pinephone_defconfig
  make CROSS_COMPILE=or1k-elf- build/scp/scp.bin
  cp build/scp/scp.bin ../u-boot-${pkgver/rc/-rc}

  cd ../arm-trusted-firmware-${_tfaver}

  echo -e "\nBuilding TF-A for Pine64 PinePhone...\n"
  make PLAT=sun50i_a64 DEBUG=0 bl31 SUNXI_SETUP_REGULATORS=1 SUNXI_AMEND_DTB=1 LOG_LEVEL=LOG_LEVEL_INFO
  cp build/sun50i_a64/release/bl31.bin ../u-boot-${pkgver/rc/-rc}

  cd ../u-boot-${pkgver/rc/-rc}

  echo -e "\nBuilding U-Boot for Pine64 PinePhone...\n"
  make pinephone_defconfig

  update_config 'CONFIG_IDENT_STRING' '" Manjaro Linux ARM"'
  update_config 'CONFIG_BOOTDELAY' '0'
  update_config 'CONFIG_SERIAL_PRESENT' 'y'
  update_config 'CONFIG_GZIP' 'y'
  update_config 'CONFIG_CMD_UNZIP' 'y'
  update_config 'CONFIG_CMD_EXT4' 'y'
  update_config 'CONFIG_SUPPORT_RAW_INITRD' 'y'
  update_config 'CONFIG_CMD_EXT4_WRITE' 'n'
  update_config 'CONFIG_EXT4_WRITE' 'n'
  update_config 'CONFIG_OF_LIBFDT_OVERLAY' 'y'

  # Build U-Boot images for different RAM speeds
  local RAM_MHZ
  for RAM_MHZ in 492 528 552 592 624; do
    echo -e "\nBuilding U-Boot variant that runs RAM at ${RAM_MHZ} MHz...\n"
    update_config 'CONFIG_DRAM_CLK' "${RAM_MHZ}"
    make EXTRAVERSION=-${pkgrel}
    cp -a u-boot-sunxi-with-spl.bin "u-boot-sunxi-with-spl-pinephone-${RAM_MHZ}.bin"
  done
}   

package() {
  cd u-boot-${pkgver/rc/-rc}

  install -D -m 0644 u-boot-sunxi-with-spl-pinephone{-624,-592,-552,-528,-492}.bin -t "${pkgdir}/boot"
  install -D -m 0644 "${srcdir}/boot.txt" -t "${pkgdir}/boot"
  install -D -m 0755 "${srcdir}/pp-uboot-mkscr" -t "${pkgdir}/usr/bin"

  install -D -m 0755 "${srcdir}/pp-prepare-fstab" -t "${pkgdir}/usr/bin"
  install -D -m 0644 "${srcdir}/pp-prepare-fstab.service" -t "${pkgdir}/usr/lib/systemd/system"
  install -D -m 0755 "${srcdir}/pp-uboot-flash" -t "${pkgdir}/usr/bin"
  install -D -m 0644 "${srcdir}/uboot.conf" -t "${pkgdir}/etc/pinephone"
}
